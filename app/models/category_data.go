package models

import "log"

var (
	categories = []Category{
		{
			"Networking",
			"networking",
			"/category/networking",
			"/public/img/category/networking.jpg",
			"Calculations that you might use for Networking/Computer Science: Binary to Decimal, Binary " +
				"to Hexadecimal, Decimal to Binary, Decimal to Hexadecimal, Hexadecimal to Binary, and " +
				"Hexadecimal to Decimal.",
			getCalculationsForCategory("Networking"),
		},
		{
			"Primes and Factors",
			"primes-and-factors",
			"/category/primes-and-factors",
			"/public/img/category/numbers.jpg",
			"Calculations about numbers! Find Highest Common Factor, find Lowest Common Multiple, and " +
				"figuring out Prime Numbers.",
			getCalculationsForCategory("Primes and Factors"),
		},
		{
			"Percentages",
			"percentages",
			"/category/percentages",
			"/public/img/category/percentages.jpg",
			"Calculations for percentages! Find the value from a percentage, find a percentage from a " +
				"value, or find the percentage change between two values.",
			getCalculationsForCategory("Percentages"),
		},
		{
			"Total Surface Area",
			"total-surface-area",
			"/category/total-surface-area",
			"/public/img/category/tsa.jpg",
			"Calculations that you might use for Total Surface Area: Pythagorean Theorem (also known as " +
				"Pythagoras's Theorem), Total Surface Area of Cone, Total Surface Area of Cube, Total Surface " +
				"Area of Cylinder, Total Surface Area of Rectangular Prism, Total Surface Area of Sphere, and " +
				"Total Surface Area of Square Based Triangle.",
			getCalculationsForCategory("Total Surface Area"),
		},
	}
)

func getCalculationsForCategory(category string) []Calculation {
	calcs, err := getCalculationsByCategoryName(category)
	if err != nil {
		log.Fatalln(err.Error())
	}
	return calcs
}
