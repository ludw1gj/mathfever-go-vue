package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/robertjeffs/mathfever-go-vue/app/controllers"
)

// Load returns a router instance with routes and file servers.
func Load() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	apiRouter := router.PathPrefix("/api").Subrouter()

	// file servers
	spaFileServer := http.FileServer(http.Dir("./dist"))
	router.PathPrefix("/dist/").Handler(http.StripPrefix("/dist/", spaFileServer))

	publicFileServer := http.FileServer(http.Dir("./public"))
	router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", publicFileServer))

	// routes
	setAPIRoutes(apiRouter)
	router.PathPrefix("/").HandlerFunc(spaHandler("./index.html")).Methods("GET")

	return router
}

func spaHandler(file string) func(w http.ResponseWriter, r *http.Request) {
	serveFileHandler := func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, file)
	}
	return http.HandlerFunc(serveFileHandler)
}

func setAPIRoutes(apiRouter *mux.Router) {
	apiRouter.HandleFunc("/categories", controllers.GetCategoriesHandler).Methods("GET")
	apiRouter.HandleFunc("/category", controllers.GetCategoryHandler).Methods("GET")
	apiRouter.HandleFunc("/calculation", controllers.GetCalculationHandler).Methods("GET")
	apiRouter.HandleFunc("/calculation", controllers.ProcessCalculationHandler).Methods("POST")
	apiRouter.NotFoundHandler = http.HandlerFunc(controllers.NotFoundHandler)
}
