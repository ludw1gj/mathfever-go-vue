import axios from "axios/index";
import displayCalculationCard from "./display"

export const submitAjax = (data, calculationSlug) => {
  const spinner = document.getElementById("loading-spinner-container");

  axios.post(`/api/calculation?calculation=${calculationSlug}`, data)
    .then(response => {
      spinner.style.opacity = "0";

      if (response.status >= 200 && response.status <= 404) {
        const resp = response.data;

        if (resp.error) {
          displayCalculationCard(resp.error);
        } else {
          displayCalculationCard(resp.content);
        }
      }
      else if (response.status === 429) {
        displayCalculationCard("You sent too many requests to the server, come back tomorrow.")
      } else {
        displayCalculationCard("The server has encountered a problem.");
      }
    })
    .catch(error => {
      spinner.style.opacity = "0";
      displayCalculationCard("There was a connection issue. Check your internet connection or the sever might be down.")
    })
};

export default {
  submitAjax
}
