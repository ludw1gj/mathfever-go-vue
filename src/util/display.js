export const displayCalculationCard = (content) => {
  const calculationCard = document.getElementById("calculation-card");
  calculationCard.innerHTML = "<p>" + content + "</p>";
  calculationCard.classList.add("fade-in");
};

export default {
  displayCalculationCard
}
