import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import vueHeadful from 'vue-headful'

import NotFoundPage from './components/NotFoundPage'
import HomePage from './components/HomePage'
import CategoryPage from './components/CategoryPage'
import AboutPage from './components/AboutPage'
import HelpPage from './components/HelpPage'
import TermsPage from './components/TermsPage'
import PrivacyPage from './components/PrivacyPage'
import MessageBoardPage from './components/MessageBoardPage'
import ConversionTablePage from './components/ConversionTablePage'
import CalculationPage from './components/CalculationPage'

Vue.component('vue-headful', vueHeadful)
Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "home-page",
    component: HomePage
  },
  {
    path: "/category/:slug",
    component: CategoryPage
  },
  {
    path: "/about",
    name: "about-page",
    component: AboutPage
  },
  {
    path: "/help",
    name: "help-page",
    component: HelpPage
  },
  {
    path: "/message-board",
    name: "message-board-page",
    component: MessageBoardPage
  },
  {
    path: "/terms",
    name: "terms-page",
    component: TermsPage
  },
  {
    path: "/privacy",
    name: "privacy-page",
    component: PrivacyPage
  },
  {
    path: "/category/networking/conversion-table",
    name: "conversion-table-page",
    component: ConversionTablePage
  },
  {
    path: "/category/:categorySlug/:calculationSlug",
    name: "calculation-page",
    component: CalculationPage
  },
  {
    path: "*",
    name: "not-found-page",
    component: NotFoundPage
  }
]

const router = new VueRouter({
  mode: 'history',
  routes: routes,
  scrollBehavior(to, from, savedPosition) {
    return {x: 0, y: 0}
  }
})

new Vue({
  el: '#app',
  router: router,
  render: h => h(App)
})
