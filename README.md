# Mathfever (Golang, Vue.js)

A reimplementation of MathFever website, removing the responsibility
of the front-end from the server (Golang). Using Vue.js, the front-end is
a Single Page Application which queries the Golang server via AJAX, making
the Golang code only responsible for API routes and sending the SPA to
the client.

## Project Dependencies
Golang >=1.10<br>
gorilla/mux

## Setup Notes

``` bash
# Get dependancies
go get github.com/gorilla/mux
go get github.com/ludw1gj/mathfever-go-vue
q
# Navigate to the mathfever-go-vue directory
cd {your go path}/github.com/ludw1gj/mathfever-go-vue

# Install dependencies for the vue.js build
npm install

# Serve with hot reload at localhost:8080 (without the API availability)
npm run dev

# Build for production with minification
npm run build

# To run the project (access via localhost:8000)
go run main.go

# To build the project (access via localhost:8000)
go build
./mathfever-go-vue
```
