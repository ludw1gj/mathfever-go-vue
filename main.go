package main

import (
  "github.com/robertjeffs/mathfever-go-vue/app/router"
  "log"
  "net/http"
)

func main() {
  log.Println("site listening on port 8000...")
  log.Fatal(http.ListenAndServe(":8000", router.Load()))
}
